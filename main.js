// Modules to control application life and create native browser window
const { app, BrowserWindow, BrowserView } = require('electron')
const path = require('path')

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  });

  const SHARED_WORKER_DEMO_URL = `http://mdn.github.io/simple-shared-worker/index2.html`;

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  let view = new BrowserView({
    webPreferences: {
      devTools: true
    }
  });
  mainWindow.addBrowserView(view);
  const windowBounds = mainWindow.getBounds();
  view.setBounds({ x: 0, y: windowBounds.height / 2, width: windowBounds.width, height: windowBounds.height / 2 });
  view.webContents.loadURL(SHARED_WORKER_DEMO_URL);
  
  view.webContents.on('before-input-event', (e, input) => {
    if (input.key === 'Enter') {
      const workers = view.webContents.getAllSharedWorkers();
      view.webContents.inspectSharedWorkerById(workers[0].id);
    }
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
